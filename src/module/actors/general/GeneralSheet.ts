import { GeneralData } from "./GeneralData";
import { weaponItemType } from "../../items/weapon/weaponData";
import { SystemRoll } from "../../rolls/SystemRoll";

export class GeneralSheet extends ActorSheet<Actor<GeneralData>, GeneralData> {
    static get defaultOptions() {
        return mergeObject(super.defaultOptions, {
            classes: ["beyond-the-wall", "sheet", "general"],
            template: "systems/beyond-the-wall/templates/general.hbs",
            width: 520,
            height: 450,
            dragDrop: [{ dragSelector: ".item-list .item", dropSelector: null }],
        });
    }

    getData() {
        var data = super.getData();

        data.data.carryWeight = Math.decimals(
            data.items
                .filter((i) => i.data.weight != undefined)
                .filter((i) => i.data.wearing)
                .map((i) => Number.parseFloat(i.data.weight) * Number.parseInt(i.data.quantity ?? 1))
                .reduce((p, c) => p + c, 0),
            3
        );
        return data;
    }

    /** @override */
    setPosition(options = {}) {
        const position = super.setPosition(options);
        const sheetBody = this.element.find(".sheet-body");
        const bodyHeight = position.height - 150;
        sheetBody.css("height", bodyHeight);
        return position;
    }

    /** @override */
    activateListeners(html: JQuery) {
        super.activateListeners(html);

        // Everything below here is only needed if the sheet is editable
        if (!this.options.editable) return;

        html.find(".rollable").click(async (ev) => {
            let target = $(ev.currentTarget).data();
            let item = this.actor.items.get(target.itemId);
            let roll: Roll;
            let mod: number;
            let flavor: string = "";
            if (item.data.data.type == weaponItemType) {
                switch (target.type) {
                    case "use":
                        mod = await SystemRoll.getModifier("Modifier");
                        roll = new SystemRoll(this.actor, this.actor.items.get(target.itemId), "attack", mod);
                        break;
                    case "damage":
                        roll = new Roll(this.actor.items.get(target.itemId).data.data.damage);
                        flavor = `<h3>${this.actor.items.get(target.itemId).name} Damage</h3>`;
                        break;
                }
            } else {
                roll = new Roll(this.actor.items.get(target.itemId).data.data.macro);
                flavor = `<h3>${this.actor.items.get(target.itemId).name}</h3>`;
            }
            roll.render().then((content) => {
                ChatMessage.create({
                    user: game.user._id,
                    speaker: ChatMessage.getSpeaker({ actor: this.actor }),
                    content: flavor + content,
                });
            });
        });
        html.find(".item-inc").click((ev) => {
            const li = $(ev.currentTarget).parents(".item");
            var item = this.actor.items.get(li.data("itemId"));
            item.update({ _id: item.id, data: { quantity: Number.parseFloat(item.data.data.quantity) + 1 } }, {});
        });

        html.find(".item-dec").click((ev) => {
            const li = $(ev.currentTarget).parents(".item");
            var item = this.actor.items.get(li.data("itemId"));
            item.update({ _id: item.id, data: { quantity: Number.parseFloat(item.data.data.quantity) - 1 } }, {});
        });

        // Update Inventory Item
        html.find(".item-edit").click((ev) => {
            const li = $(ev.currentTarget).parents(".item");
            const item = this.actor.getOwnedItem(li.data("itemId"));
            item.sheet.render(true);
        });

        // Delete Inventory Item
        html.find(".item-delete").click((ev) => {
            const li = $(ev.currentTarget).parents(".item");
            this.actor.deleteOwnedItem(li.data("itemId"));
            li.slideUp(200, () => this.render(false));
        });

        html.find(".skill-add").click(this._onClickSkillControl.bind(this));
        html.find(".skill-delete").click(this._onClickSkillControl.bind(this));
    }

    async _onClickSkillControl(event: JQuery.ClickEvent) {
        event.preventDefault();
        const a = event.currentTarget;
        const action = a.dataset.action;
        const skills = this.actor.data.data.skills;

        if (action == "add") {
            const nk = Object.keys(skills).reduce((p, c) => Math.max(p, parseInt(c)), 0) + 1;
            let newKey = document.createElement("input");
            newKey.type = "text";
            newKey.name = `data.skills.${nk}.name`;
            newKey.value = "New Statistic";
            this.form.appendChild(newKey);
            await this._onSubmit(event);
        }

        if (action == "remove") {
            $(`input[name^="data.skills.${a.dataset.index}"]`).remove();
            await this._onSubmit(event);
        }
    }

    /** @override */
    _updateObject(_: Event, formData: any) {
        const skills = expandObject(formData).data.skills || [];
        for (let k of Object.keys(this.object.data.data.skills)) {
            if (!skills.hasOwnProperty(k)) skills[`-=${k}`] = null;
        }

        // Re-combine formData
        formData = Object.entries(formData).reduce<any>(
            (obj, e) => {
                obj[e[0]] = e[1];
                return obj;
            },
            { _id: this.object._id, data: { skills } }
        );

        // Update the Item
        return this.object.update(formData);
    }
}
