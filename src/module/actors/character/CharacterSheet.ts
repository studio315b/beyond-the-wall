import { CharacterData } from "./CharacterData";
import { getModifier, AbilityScoreBlock } from "../shared/AbilityScores";
import { weaponItemType } from "../../items/weapon/weaponData";
import { spellItemType } from "../../items/spell/spellData";
import { traitItemType } from "../../items/trait/traitData";
import { armorItemType } from "../../items/armor/armorData";
import { equipmentItemType } from "../../items/equipment/equipmentData";
import { SystemRoll } from "../../rolls/SystemRoll";

export class CharacterSheet extends ActorSheet<Actor<CharacterData>, CharacterData> {
    static get defaultOptions() {
        return mergeObject(super.defaultOptions, {
            classes: ["beyond-the-wall", "sheet", "character"],
            template: "systems/beyond-the-wall/templates/character.hbs",
            width: 575,
            height: 700,
            tabs: [
                {
                    navSelector: ".sheet-tabs",
                    contentSelector: ".sheet-body",
                    initial: "main",
                },
            ],
            dragDrop: [{ dragSelector: ".item-list .item", dropSelector: null }],
        });
    }

    getData() {
        var data = super.getData();

        data.data.abilityMods = Object.entries(data.data.abilities)
            .map(([k, v]) => [k, getModifier(v)])
            .reduce<AbilityScoreBlock>((p, c) => ({ ...p, [c[0]]: c[1] }), {} as any);
        data.data.checkedFortunes = new Array(data.data.fortune).fill(true);
        data.data.checkedSlots = new Array(data.data.slots).fill(true);
        data.data.carryWeight = Math.decimals(
            data.items
                .filter((i) => i.data.weight != undefined)
                .filter((i) => i.data.wearing)
                .map((i) => Number.parseFloat(i.data.weight) * Number.parseInt(i.data.quantity ?? 1))
                .reduce((p, c) => p + c, 0),
            3
        );
        while (data.data.misc.length < 3) {
            data.data.misc.push("");
        }
        data.data.itemGroups = this.generateItemGroups(data.items);
        return data;
    }

    generateItemGroups(items: ItemSheetData[]) {
        return {
            active: items.filter((i) => i.data.wearing).filter((i) => i.data.type == weaponItemType),
            arcana: items.filter((i) => i.data.type == spellItemType),
            traits: items.filter((i) => i.data.type == traitItemType),
            weapons: items.filter((i) => i.data.type == weaponItemType),
            armor: items.filter((i) => i.data.type == armorItemType),
            equipment: items.filter((i) => i.data.type == equipmentItemType),
        };
    }

    /** @override */
    setPosition(options = {}) {
        const position = super.setPosition(options);
        const sheetBody = this.element.find(".sheet-body");
        const bodyHeight = position.height - 150;
        sheetBody.css("height", bodyHeight);
        return position;
    }

    /** @override */
    activateListeners(html: JQuery) {
        super.activateListeners(html);

        // Everything below here is only needed if the sheet is editable
        if (!this.options.editable) return;

        html.find(".rollable").click(async (ev) => {
            let target = $(ev.currentTarget).data();
            let roll: Roll;
            let mod: number;
            let flavor: string = "";
            switch (target.type) {
                case "skill":
                    mod = await SystemRoll.getModifier("Difficulty");
                    roll = new SystemRoll(this.actor, target.ability, target.type, mod);
                    break;
                case "save":
                    mod = await SystemRoll.getModifier("Modifier");
                    roll = new SystemRoll(this.actor, target.ability, target.type, mod);
                    break;
                case "damage":
                    roll = new Roll(this.actor.items.get(target.itemId).data.data.damage);
                    flavor = `<h3>${this.actor.items.get(target.itemId).name} Damage</h3>`;
                    break;
                case "attack":
                    mod = await SystemRoll.getModifier("Modifier");
                    roll = new SystemRoll(this.actor, this.actor.items.get(target.itemId), target.type, mod);
                    break;
                case "macro":
                    roll = new Roll(this.actor.items.get(target.itemId).data.data.macro);
                    flavor = `<h3>${this.actor.items.get(target.itemId).name}</h3>`;
                    break;
            }
            roll.render().then((content) => {
                ChatMessage.create({
                    user: game.user._id,
                    speaker: ChatMessage.getSpeaker({ actor: this.actor }),
                    content: flavor + content,
                });
            });
        });

        // Update Inventory Item
        html.find(".item-edit").click((ev) => {
            const li = $(ev.currentTarget).parents(".item");
            const item = this.actor.getOwnedItem(li.data("itemId"));
            item.sheet.render(true);
        });

        html.find(".item-inc").click((ev) => {
            const li = $(ev.currentTarget).parents(".item");
            var item = this.actor.items.get(li.data("itemId"));
            item.update({ _id: item.id, data: { quantity: Number.parseFloat(item.data.data.quantity) + 1 } }, {});
        });

        html.find(".item-dec").click((ev) => {
            const li = $(ev.currentTarget).parents(".item");
            var item = this.actor.items.get(li.data("itemId"));
            item.update({ _id: item.id, data: { quantity: Number.parseFloat(item.data.data.quantity) - 1 } }, {});
        });

        // Delete Inventory Item
        html.find(".item-delete").click((ev) => {
            const li = $(ev.currentTarget).parents(".item");
            this.actor.deleteOwnedItem(li.data("itemId"));
            li.slideUp(200, () => this.render(false));
        });
        html.find(".item-stash").click((ev) => {
            const li = $(ev.currentTarget).parents(".item");
            var item = this.actor.items.get(li.data("itemId"));
            item.update({ _id: item.id, data: { wearing: !item.data.data.wearing } }, {});
        });

        html.find(".skill-add").click(this._onClickSkillControl.bind(this));
        html.find(".skill-delete").click(this._onClickSkillControl.bind(this));

        html.find(".goal-add").click(this._onClickGoalControl.bind(this));
        html.find(".goal-delete").click(this._onClickGoalControl.bind(this));
    }

    async _onClickSkillControl(event: JQuery.ClickEvent) {
        event.preventDefault();
        const a = event.currentTarget;
        const action = a.dataset.action;
        const skills = this.actor.data.data.skills;

        if (action == "add") {
            const nk = Object.keys(skills).reduce((p, c) => Math.max(p, parseInt(c)), 0) + 1;
            let newKey = document.createElement("input");
            newKey.type = "text";
            newKey.name = `data.skills.${nk}.name`;
            newKey.value = "New Skill";
            this.form.appendChild(newKey);
            await this._onSubmit(event);
        }

        if (action == "remove") {
            $(`input[name^="data.skills.${a.dataset.index}"]`).remove();
            await this._onSubmit(event);
        }
    }

    async _onClickGoalControl(event: JQuery.ClickEvent) {
        event.preventDefault();
        const a = event.currentTarget;
        const action = a.dataset.action;
        const goals = this.actor.data.data.goals;

        if (action == "add") {
            const nk = Object.keys(goals).reduce((p, c) => Math.max(p, parseInt(c)), 0) + 1;
            let newKey = document.createElement("input");
            newKey.type = "text";
            newKey.name = `data.goals.${nk}.description`;
            newKey.value = "New Goal";
            this.form.appendChild(newKey);
            await this._onSubmit(event);
        }

        if (action == "remove") {
            $(`input[name^="data.goals.${a.dataset.index}"]`).remove();
            await this._onSubmit(event);
        }
    }

    /** @override */
    _updateObject(_: Event, formData: any) {
        const exp = expandObject(formData).data as CharacterData;
        const skills = exp.skills || [];
        for (let k of Object.keys(this.object.data.data.skills)) {
            if (!skills.hasOwnProperty(k)) skills[`-=${k}`] = null;
        }

        const goals = exp.goals || [];
        for (let k of Object.keys(this.object.data.data.goals)) {
            if (!goals.hasOwnProperty(k)) goals[`-=${k}`] = null;
        }

        const fortune = Object.values(exp.checkedFortunes)
            .map<number>((v) => (v ? 1 : 0))
            .reduce((p, c) => p + c);
        const slots = Object.values(exp.checkedSlots)
            .map<number>((v) => (v ? 1 : 0))
            .reduce((p, c) => p + c);
        // Re-combine formData
        formData = Object.entries(formData).reduce<any>(
            (obj, e) => {
                obj[e[0]] = e[1];
                return obj;
            },
            {
                _id: this.object._id,
                data: {
                    fortune,
                    slots,
                    skills,
                    goals,
                },
            }
        );

        // Update the Item
        return this.object.update(formData);
    }
}
