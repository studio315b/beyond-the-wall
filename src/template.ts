import { characterActorType, emptyCharacterData } from "./module/actors/character/CharacterData";
import { generalActorType, emptyGeneralData } from "./module/actors/general/GeneralData";
import { spellItemType, emptySpellData } from "./module/items/spell/spellData";
import { armorItemType, emptyArmorData } from "./module/items/armor/armorData";
import { weaponItemType, emptyWeaponData } from "./module/items/weapon/weaponData";
import { emptyEquipmentData, equipmentItemType } from "./module/items/equipment/equipmentData";
import { emptyTraitData, traitItemType } from "./module/items/trait/traitData";

export const Actors = {
    [characterActorType]: emptyCharacterData,
    [generalActorType]: emptyGeneralData,
};

export const Items = {
    [spellItemType]: emptySpellData,
    [armorItemType]: emptyArmorData,
    [weaponItemType]: emptyWeaponData,
    [equipmentItemType]: emptyEquipmentData,
    [traitItemType]: emptyTraitData,
};
